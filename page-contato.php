<?php // Template Name: Contato ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-contato">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- CONTATO -->
        <div class="contato">
            <div class="faixa">
                <h2 class="titulo-faixa">Contato</h2>
            </div>
            <div class="linha">
                <div class="info">
                    <div class="email">
                        <span>Email: </span><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>
                    </div>
                    <div class="responsaveis">
                        <!-- LOOP -->
                        <?php if(have_rows('responsaveis')): while(have_rows('responsaveis')) : the_row(); ?>
                            <h4 class="titulo"><?php the_sub_field('nome-responsavel'); ?></h4>
                            <a href="tel:<?php the_sub_field('numero-responsavel'); ?>" class="texto"><?php the_sub_field('numero-responsavel'); ?></a>
                        <?php endwhile; else : endif; ?>
                        <!-- ... -->

                        <div class="linha">
                            <div class="convercional">
                                <h4 class="titulo">Convencional</h4>
                                <a href="tel:" class="texto">81 3494.5791</a>
                            </div>
                            <div class="whatsapp">
                                <h4 class="titulo">Whatsapp</h4>
                                <a href="tel:" class="texto">81 9 8373.7392</a>
                            </div>
                        </div>
                        
                    </div>
                    <div class="endereco">
                        <span>Endereço: </span><a href="#"><?php the_field('endereco'); ?></a>
                    </div>
                    <div class="mais-info">
                        <p class="texto">CNPJ: 11.030.300/0001-30</p>
                    </div>
                </div>
                <div class="formulario">
                    <p class="texto">Para maiores informações, preencha os campos com seus dados e em breve retornaremos seu contato. Se preferir, entre em contato conosco.</p>
                     <?php echo do_shortcode('[contact-form-7 id="284" title="Contato"]'); ?>
                </div>
            </div>
            <div class="mapa">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.0406349050068!2d-34.87559535704797!3d-7.994740809493215!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab17f075ba8ed9%3A0xfe3159614df98054!2sCreche%20Manoel%20Quint%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1606106909836!5m2!1spt-BR!2sbr" width="100%" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>