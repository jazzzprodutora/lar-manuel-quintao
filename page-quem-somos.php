<?php // Template Name: Quem Somos ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-quem-somos">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- QUEM SOMOS -->
        <div class="quem-somos">
            <div class="faixa">
                <h2 class="titulo-faixa">Quem Somos</h2>
            </div>
            <div class="conteudo">
                <div class="cont-quem-somos">
                    <div class="imagem-destaque">
                        <img src="<?php the_field('imagem-destaque'); ?>">
                    </div>
                    <div class="texto-destaque">
                        <div class="itens">

                            <!-- LOOP -->
                            <?php if(have_rows('itens-quem-somos')): while(have_rows('itens-quem-somos')) : the_row(); ?>

                            <div class="item">
                                <h2 class="titulo-area"><?php the_sub_field('titulo'); ?></h2>
                                <p><?php the_sub_field('texto'); ?></p>
                            </div>

                            <?php endwhile; else : endif; ?>
                            <!-- ... -->

                        </div>
                        
                    </div>
                </div>
                <div class="mais-imagens">

                    <!-- LOOP -->
                    <?php if(have_rows('mais-imagens')): while(have_rows('mais-imagens')) : the_row(); ?>
                    <img src="<?php the_sub_field('imagem'); ?>">
                    <?php endwhile; else : endif; ?>
                    <!-- ... -->

                </div>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>