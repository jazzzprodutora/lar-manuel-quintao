<?php // Template Name: Doação ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-doacao">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- DOACAO -->
        <div class="doacao">
            <div class="faixa">
                <h2 class="titulo-faixa">Doações</h2>
            </div>
            <div class="conteudo">
                <h3 class="titulo">Ajude doando qualquer quantia</h3>
                <p class="texto"><?php the_field('texto-doacoes'); ?></p>
                <div class="itens">
                    <div class="item">
                        <img class="img-titulo" src="<?php echo get_stylesheet_directory_uri(); ?>/icons/paypal.png">
                        <h4>Paypal</h4>
                        <p>Utilize o PayPal para fazer a sua doação</p>
                        <div class="area-botao">
                            <a href="https://www.paypal.com/donate/?hosted_button_id=GKWFECM38DKMY" class="botao botao-secundario">Doar com PayPal</a>
                        </div>
                        <img class="qr-code" src="<?php echo get_stylesheet_directory_uri(); ?>/icons/qrcode.png" alt="">
                    </div>
                    <!-- ... -->
                    <div class="item">
                        <img class="img-titulo"  src="<?php echo get_stylesheet_directory_uri(); ?>/icons/pag-seguro.png">
                        <h4>PagSeguro</h4>
                        <p>Utilize o PagSeguro para fazer a sua doação.</p>
                        <div class="info-doacao">


                        </div>
                        <div class="area-botao">
                            <a href="https://pagseguro.uol.com.br/checkout/nc/nl/donation/sender-identification.jhtml?t=c9ec23abef733c980b0480f799aa4fef2cc08feadcf49be0793ed655beb43fd1&e=true" class="botao botao-secundario">Doar com PagSeguro</a>
                        </div>
                    </div>
                    <div class="item">
                        <img class="img-titulo"  src="<?php echo get_stylesheet_directory_uri(); ?>/icons/bb.png">
                        <h4>Banco do Brasil</h4>
                        <p>Utilize o Banco do Brasil para fazer a sua doação através de transferência bancária, TED.</p>
                        <div class="info-doacao">
                            <p><span>Agência:</span>4274-9</p>
                            <p><span>Conta Corrente:</span>10.733-6</p>
                            <p><span>CNPJ:</span>11.030.300/0001-30</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>