let menu = document.querySelector('.layer .menu');
let redesSociais = document.querySelector('.layer .redes-sociais');
let props = document.querySelector('.props');
let layer = document.querySelector('.layer');
let prop1 = document.querySelector('.prop1');
let prop2 = document.querySelector('.prop2');
let prop3 = document.querySelector('.prop3');
props.addEventListener('click', () => {
  layer.classList.toggle('ativa');
  menu.classList.toggle('visivel');
  redesSociais.classList.toggle('visivel');
  prop1.classList.toggle('anima-prop1');
  prop2.classList.toggle('anima-prop2');
  prop3.classList.toggle('anima-prop3');
});
