<?php // Template Name: Galeria ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-galeria">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- GALERIA -->
        <div class="galeria">
            <div class="faixa">
                <h2 class="titulo-faixa">Galeria</h2>
            </div>

            <div class="conteudo">
                <div class="itens">
                    <!-- ... -->
                    <?php
                        $args = array (
                            'post_type' => 'galeria-fotos', //Pega os post types no array para ser mostrado nos post
                            'posts_per_page'=> -1
                        );
                        $the_query = new WP_Query ( $args );
                    ?>
                    <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                    <div class="item">
                        <a href="<?php the_permalink();?>">
                            <div class="imagem-item">
                                <?php the_post_thumbnail()?>
                            </div>
                            <div class="titulo-item">
                                <h2 class="titulo"><?php the_title()?></h2>
                            </div>
                        </a>
                    </div>

                    <?php endwhile; else: endif; ?>
                    <!-- ... -->
                </div>
            </div>
            
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>