<?php // Template Name: Home ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <!-- HEADER -->
    <section class="header">
        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>
        
        <!-- DIVISOR -->
        <?php require 'templates/divisor.php' ?>

        <!-- BANNER -->
        <div class="banner">
            <div id="carouselBanner" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">

                    <!-- LOOP -->
                    <?php if(have_rows('carousel-banner')): while(have_rows('carousel-banner')) : the_row(); ?>

                    <div class="carousel-item">
                        <img src="<?php the_sub_field('imagem'); ?>">
                        <div class="carousel-caption">
                            <h2 class="titulo"><?php the_sub_field('titulo'); ?></h2>
                            <p class="texto"><?php the_sub_field('texto'); ?></p>
                            <div class="area-botao">
                                <a href="voluntarios">
                                    <button class="botao botao-principal">Seja um voluntário</button>
                                </a>
                                <a href="doacao">
                                    <button class="botao botao-secundario">Faça uma doação</button>
                                </a>
                            </div>
                        </div>
                    </div>

                    <?php endwhile; else : endif; ?>
                    <!-- FIM DO LOOP -->

                </div>
                <a class="carousel-control-prev" href="#carouselBanner" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#carouselBanner" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Proximo</span>
                </a>
            </div>
        </div>
    </section>

    <!-- PREVIA QUEM SOMOS -->
    <section class="previa-quem-somos">
        <div class="linha">
            <div class="area-texto">
                <h1 class="titulo"><?php the_field('previa-quem-somos-titulo'); ?></h1>
                <p class="texto"><?php the_field('previa-quem-somos-texto'); ?></p>
            </div>
            <div class="area-imagem">
                <img src="<?php the_field('previa-quem-somos-imagem'); ?>" alt="<?php the_field('previa-quem-somos-titulo'); ?>">
            </div>
        </div>
        <div class="area-botao">
            <a href="voluntarios">
                <button class="botao botao-principal">Seja um voluntário</button>
            </a>
            <a href="doacao">
                <button class="botao botao-secundario">Faça uma doação</button>
            </a>
        </div>
    </section>

    <!-- CHAMADA -->
    <section class="chamada">
        <div class="itens">
            <div class="item">
                <h2 class="titulo"><?php the_field('chamada1-titulo'); ?></h2>
                <p class="texto"><?php the_field('chamada1-texto'); ?></p>
            </div>
            <div class="item">
                <h2 class="titulo"><?php the_field('chamada2-titulo'); ?></h2>
                <p class="texto"><?php the_field('chamada2-texto'); ?></p>
            </div>
            <div class="item">
                <h2 class="titulo"><?php the_field('chamada3-titulo'); ?></h2>
                <p class="texto"><?php the_field('chamada3-texto'); ?></p>
            </div>
        </div>
    </section>

    <!-- PREVIA GALERIA DE FOTOS -->
    <section class="previa-galeria">      
        <div class="linha">
            <h1 class="titulo">Galeria de fotos</h1>
            <a href="galeria">>> Ver tudo</a>
        </div>
        <div class="itens">
            <!-- ... -->
            <?php
                $args = array (
                    'post_type' => 'galeria-fotos', //Pega os post types no array para ser mostrado nos post
                    'posts_per_page'=> 2
                );
                $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <div class="item">
                <a href="<?php the_permalink();?>">
                    <div class="imagem-item">
                        <?php the_post_thumbnail()?>
                    </div>
                    <div class="titulo-item">
                        <h2 class="titulo"><?php the_title()?></h2>
                    </div>
                </a>
            </div>

            <?php endwhile; else: endif; ?>
            <!-- ... -->
        </div>
        <!-- <div class="accordion" id="accordionGaleria">
            <div class="accordion-item">
                <h2 class="accordion-header" id="item0">
                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse0" aria-expanded="true" aria-controls="collapse0">
                        2020
                    </button>
                </h2>
                CONTEUDO
                <div id="collapse0" class="accordion-collapse collapse show" aria-labelledby="item0" data-bs-parent="#accordionGaleria">
                    <div class="accordion-body">
                        IMAGENS DA GALERIA
                        <?php echo do_shortcode( '[rl_gallery id="9"]' ); ?>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="item1">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                        2019
                    </button>
                </h2>
                <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="item1" data-bs-parent="#accordionGaleria">
                    <div class="accordion-body">
                        IMAGENS DA GALERIA
                        <?php echo do_shortcode( '[rl_gallery id="9"]' ); ?>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="item2">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                        2018
                    </button>
                </h2>
                <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="item2" data-bs-parent="#accordionGaleria">
                    <div class="accordion-body">
                        IMAGENS DA GALERIA
                        <?php echo do_shortcode( '[rl_gallery id="9"]' ); ?>
                    </div>
                </div>
            </div> 
        </div> -->
    </section>

    <!-- PREVIA NOTICIAS -->
    <section class="previa-noticias">
        <div class="linha">
            <h1 class="titulo">Notícias</h1>
            <a href="noticias">>> Ver todas as notícias</a>
        </div>
        <div class="itens">

            <!-- ... -->
            <?php
                $args = array (
                    'post_type' => 'noticia', //Pega os post types no array para ser mostrado nos post
                    'posts_per_page'=> 2
                );
                $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <div class="item">
                <a href="<?php the_permalink();?>">
                    <div class="imagem-item">
                        <?php the_post_thumbnail()?>
                    </div>
                    <div class="data">
                        <span><?php the_field('data'); ?></span>
                    </div>
                    <div class="titulo-item">
                        <h2 class="titulo"><?php the_title()?></h2>
                    </div>
                </a>
            </div>

            <?php endwhile; else: endif; ?>
            <!-- ... -->
            
        </div>
    </section>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>