<?php // Template Name: Transparência ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-transparencia">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- TRANSPARENCIA -->
        <div class="transparencia">
            <div class="faixa">
                <h2 class="titulo-faixa">Transparência</h2>
            </div>
            <div class="conteudo">
                <p class="texto"><?php the_field('texto-transparencia'); ?></p>
                <div class="itens">

                    <!-- LOOP -->
                    <?php if(have_rows('itens-transparencia')): while(have_rows('itens-transparencia')) : the_row(); ?>
                    <div class="item">
                        <h3 class="titulo"><?php the_sub_field('titulo'); ?></h3>
                        <div class="linha">

                            <!-- LOOP -->
                            <?php if(have_rows('itens-lista')): while(have_rows('itens-lista')) : the_row(); ?>
                            
                            <p class="texto"><?php the_sub_field('item'); ?></p>

                            <?php endwhile; else : endif; ?>
                            <!-- ... -->

                        </div>
                    </div>
                    <?php endwhile; else : endif; ?>
                    <!-- ... -->
                    
                </div>
            </div>

            <!-- PRESTACAO DE CONTAS POR ANO -->
            <div class="prestacao-ano">
                <h2 class="titulo">Prestação de Contas por Ano</h2>
                <div class="accordion" id="accordionPrestacaoDeContas">
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="item0">
                            <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseZero" aria-expanded="true" aria-controls="collapseZero">
                                2021
                            </button>
                        </h2>
                        <div id="collapseZero" class="accordion-collapse collapse show" aria-labelledby="item0" data-bs-parent="#accordionPrestacaoDeContas">
                            <div class="accordion-body">
                                <a href="#">Janeiro</a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="item1">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                2020
                            </button>
                        </h2>
                        <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="item1" data-bs-parent="#accordionPrestacaoDeContas">
                            <div class="accordion-body">
                                <a href="#">Janeiro</a>
                                <a href="#">Fevereiro</a>
                                <a href="#">Março</a>
                                <a href="#">Abril</a>
                                <a href="#">Maio</a>
                                <a href="#">Junho</a>
                                <a href="#">Julho</a>
                                <a href="#">Agosto</a>
                                <a href="#">Setembro</a>
                                <a href="#">Outubro</a>
                                <a href="#">Novembro</a>
                                <a href="#">Dezembro</a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="item2">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                2019
                            </button>
                        </h2>
                        <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="item2" data-bs-parent="#accordionPrestacaoDeContas">
                            <div class="accordion-body">
                                <a href="#">Janeiro</a>
                                <a href="#">Fevereiro</a>
                                <a href="#">Março</a>
                                <a href="#">Abril</a>
                                <a href="#">Maio</a>
                                <a href="#">Junho</a>
                                <a href="#">Julho</a>
                                <a href="#">Agosto</a>
                                <a href="#">Setembro</a>
                                <a href="#">Outubro</a>
                                <a href="#">Novembro</a>
                                <a href="#">Dezembro</a>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header" id="item3">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                2018
                            </button>
                        </h2>
                        <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="item3" data-bs-parent="#accordionPrestacaoDeContas">
                            <div class="accordion-body">
                                <a href="#">Janeiro</a>
                                <a href="#">Fevereiro</a>
                                <a href="#">Março</a>
                                <a href="#">Abril</a>
                                <a href="#">Maio</a>
                                <a href="#">Junho</a>
                                <a href="#">Julho</a>
                                <a href="#">Agosto</a>
                                <a href="#">Setembro</a>
                                <a href="#">Outubro</a>
                                <a href="#">Novembro</a>
                                <a href="#">Dezembro</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>