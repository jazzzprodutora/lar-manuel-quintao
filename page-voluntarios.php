<?php // Template Name: Voluntários ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-voluntarios">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- VOLUNTARIOS -->
        <div class="voluntarios">
            <div class="faixa">
                <h2 class="titulo-faixa">Nossos Voluntários</h2>
            </div>
            <div class="conteudo">

                <!-- EQUIPES -->
                <div class="equipes">
                    <h3 class="titulo">Equipes</h3>
                    <p class="texto"><?php the_field('texto-equipes'); ?></p>
                    <div class="itens">

                        <!-- LOOP -->
                        <?php if(have_rows('equipes')): while(have_rows('equipes')) : the_row(); ?>
                        <div class="item">
                            <img src="<?php the_sub_field('icone-equipe'); ?>">
                            <h4><?php the_sub_field('nome-equipe'); ?></h4>
                        </div>
                        <?php endwhile; else : endif; ?>
                        <!-- ... -->

                    </div>
                </div>
                
                <!-- ATIVIDADES -->
                <div class="atividades">
                    <h3 class="titulo">Atividades para arrecadação de recursos:</h3>
                    <!-- CARDS -->
                    <div class="wrapper">
                        <div class="cols">

                            <!-- LOOP -->
                            <?php if(have_rows('cards-animados')): while(have_rows('cards-animados')) : the_row(); ?>

                            <div class="col" ontouchstart="this.classList.toggle('hover');">
                                <div class="container">
                                    <div class="front" style="background-image: url('<?php the_sub_field('background'); ?>')">
                                        <div class="inner">
                                            <p><?php the_sub_field('titulo'); ?></p>
                                        </div>
                                    </div>
                                    <div class="back">
                                        <div class="inner">
                                            <p><?php the_sub_field('texto'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                            
                        </div>
                    </div>
                    <!-- FIM DOS CARDS -->

                </div>

                <!-- FORMULARIO -->
                <div class="atividades">
                    <h3 class="titulo">Preencha o Formulário e seja um voluntário</h3>
                    <?php echo do_shortcode('[contact-form-7 id="285" title="Voluntário"]'); ?>
                </div>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>