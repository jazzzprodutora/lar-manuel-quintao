<?php // Template Name: Apadrinhamento ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-apadrinhamento">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- APADRINHAMENTO -->
        <div class="apadrinhamento">
            <div class="faixa">
                <h2 class="titulo-faixa">Apadrinhamento</h2>
            </div>
            <div class="conteudo">
                <h2 class="titulo">Apoie sendo padrinho ou madrinha</h2>
                <p class="texto"><?php the_field('texto-apadrinhamento'); ?></p>
                <div class="formulario">
                    <form action="">
                        <input type="text" placeholder="Nome">
                        <input type="tel" placeholder="Telefone">
                        <input type="email" placeholder="Email">
                        <input type="text" placeholder="Assunto" value="Apadrinhamento" disabled>
                    </form>
                    <div class="area-botao">
                        <a href="#">
                            <button class="botao botao-secundario">Enviar</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>