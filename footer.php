    <!-- RODAPE -->
    <footer class="rodape">
        <!-- WHATSAPP -->
        <div class="whatsapp-fixo">
            <a href="https://api.whatsapp.com/send?phone=5581983737392&text=" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-fixo.svg" alt="Whatsapp">
            </a>
        </div>

        <!-- DIVISOR -->
        <?php require 'templates/divisor.php' ?>

        <!-- NAVEGACAO -->
        <nav class="navegacao">
            <div class="itens">
                <div class="logo">
                    <a href="home">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/logo.png" alt="Lar Manuel Quintão">
                    </a>
                </div>
                <div class="menu">
                    <a href="quem-somos" class="link-rodape">Quem Somos</a>
                    <a href="voluntarios" class="link-rodape">Voluntários</a>
                    <a href="doacao" class="link-rodape">Doações</a>
                    <a href="apadrinhamento" class="link-rodape">Apadrinhamento</a>
                    <a href="transparencia" class="link-rodape">Transparência</a>
                    <a href="galeria" class="link-rodape">Galeria</a>
                    <a href="noticias" class="link-rodape">Notícias</a>
                    <a href="contato" class="link-rodape">Contato</a>
                </div>
                <div class="redes-sociais">
                    <a href="https://api.whatsapp.com/send?phone=5581983737392&text=" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.svg" alt="Whatsapp">
                    </a>
                    <a href="https://www.facebook.com/larmanuelquintao/" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/facebook.svg" alt="Facebook">
                    </a>
                    <a href="https://www.instagram.com/larmanuelquintao/" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.svg" alt="Instagram">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgQx5PTW3n5QAzFBe_0_Xvg" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/youtube.svg" alt="Youtube">
                    </a>
                </div>
            </div>
        </nav>
        <div class="info">
            <div class="endereco">
                <a href="https://www.google.com.br/maps/place/Creche+Manoel+Quint%C3%A3o,+Olinda+-+PE/@-7.9948016,-34.9355918,12z/data=!4m5!1m2!2m1!1sCreche+Manoel+Quint%C3%A3o!3m1!1s0x7ab17f075ba8ed9:0xfe3159614df98054" target="_blank" class="texto">Rua Alameda Manuel Quintão, S/N – Sapucaia – Olinda – PE CNPJ: 11.030.300/0001-30</a>
            </div>
            <div class="linha">
                <div class="email">
                    <a href="mailto:crechemanuelquintao@gmail.com" class="texto">crechemanuelquintao@gmail.com</a>
                </div>
                <div class="telefone d-flex align-items-center">
                    <a href="tel:8134945791" class="texto mr-2">81.3494.5791 </a>
                    <a href="tel:81983737392" class="texto ml-2"> 81.98373.7392</a>

                </div>
            </div>
        </div>
    </footer>


	<!-- CHAMA O JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu-mobile.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>

	<?php wp_footer(); ?>
</body>
</html>
