<?php // Template Name: Single Galeria ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="single-galeria">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- SINGLE GALERIA -->
        <div class="galeria">
            <div class="imagem-galeria">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="titulo-noticia">
                <h2 class="titulo"><?php the_title()?></h2>
            </div>
            <div class="fotos">
                <?php echo do_shortcode( "[rl_gallery id=9]" ); ?>
            </div>
        </div>
    </div>
    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>