<?php // Template Name: Notícias ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="page-noticias">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- NOTICIAS -->
        <div class="noticias">
            <div class="faixa">
                <h2 class="titulo-faixa">Notícias</h2>
            </div>
            <div class="itens">
                <!-- ... -->
                <?php
                    $args = array (
                        'post_type' => 'noticia', //Pega os post types no array para ser mostrado nos post
                        'posts_per_page'=> -1
                    );
                    $the_query = new WP_Query ( $args );
                ?>
                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                <div class="item">
                    <a href="<?php the_permalink();?>">
                        <div class="imagem-item">
                            <?php the_post_thumbnail()?>
                        </div>
                        <div class="data">
                            <span><?php the_field('data'); ?></span>
                        </div>
                        <div class="titulo-item">
                            <h2 class="titulo"><?php the_title()?></h2>
                        </div>
                    </a>
                </div>

                <?php endwhile; else: endif; ?>
                <!-- ... -->
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>