<?php // Template Name: Single Notícia ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <div class="single-noticia">
        <!-- HEADER -->
        <section class="header">
            <!-- CABECALHO -->
            <?php require 'templates/cabecalho.php' ?>
            
            <!-- DIVISOR -->
            <?php require 'templates/divisor.php' ?>
        </section>

        <!-- SINGLE NOTICIA -->
        <div class="noticia">
            <div class="imagem-noticia">
                <?php the_post_thumbnail(); ?>
            </div>
            <div class="info">
                <div class="mais-info">
                    <span class="data"><?php the_field('data'); ?></span>
                </div>
                <div class="titulo-noticia">
                    <h2 class="titulo"><?php the_title()?></h2>
                </div>
                <div class="texto-noticia">
                    <p class="texto"><?php the_field('texto-noticia'); ?></p>
                </div>
            </div>
        </div>
    </div>
    

    

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>