<nav class="cabecalho">
    <div class="itens">
        <div class="logo">
            <a href="home">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/logo.png" alt="Lar Manuel Quintão">
            </a>
        </div>
        <div class="menu">
            <a href="quem-somos" class="link">Quem Somos</a>
            <a href="voluntarios" class="link">Voluntários</a>
            <a href="doacao" class="link">Doações</a>
            <a href="apadrinhamento" class="link">Apadrinhamento</a>
            <a href="transparencia" class="link">Transparência</a>
            <a href="galeria" class="link">Galeria</a>
            <a href="noticias" class="link">Notícias</a>
            <a href="contato" class="link">Contato</a>
        </div>
        <div class="redes-sociais">
            <a href="https://api.whatsapp.com/send?phone=81983737392&text=" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.svg" alt="Whatsapp">
            </a>
            <a href="https://www.facebook.com/larmanuelquintao/" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/facebook.svg" alt="Facebook">
            </a>
            <a href="https://www.instagram.com/larmanuelquintao/" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.svg" alt="Instagram">
            </a>
            <a href="https://www.youtube.com/channel/UCgQx5PTW3n5QAzFBe_0_Xvg" target="_blank">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/youtube.svg" alt="Youtube">
            </a>
        </div>
        <div class="menu-mobile">
            <div class="props">
                <div class="prop1"></div>
                <div class="prop2"></div>
                <div class="prop3"></div>
            </div>
            <div class="layer">
                <div class="menu">
                    <a href="quem-somos" class="link">Quem Somos</a>
                    <a href="voluntarios" class="link">Voluntários</a>
                    <a href="doacao" class="link">Doações</a>
                    <a href="apadrinhamento" class="link">Apadrinhamento</a>
                    <a href="transparencia" class="link">Transparência</a>
                    <a href="noticias" class="link">Notícias</a>
                    <a href="contato" class="link">Contato</a>
                </div>
                <div class="redes-sociais">
                    <a href="https://api.whatsapp.com/send?phone=81983737392&text=" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.svg" alt="Whatsapp">
                    </a>
                    <a href="https://www.facebook.com/larmanuelquintao/" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/facebook.svg" alt="Facebook">
                    </a>
                    <a href="https://www.instagram.com/larmanuelquintao/" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.svg" alt="Instagram">
                    </a>
                    <a href="https://www.youtube.com/channel/UCgQx5PTW3n5QAzFBe_0_Xvg" target="_blank">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/youtube.svg" alt="Youtube">
                    </a>
                </div>
            </div>
        </div>
    </div>
</nav>